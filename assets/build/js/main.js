// modal // 

const tearmsBtn = document.querySelector('.tearms__btn');
const modal = document.querySelector('.modal');
const closeBtn = document.querySelector('.header__close');

tearmsBtn.addEventListener('click', () => {
    modal.classList.toggle('active');
})

closeBtn.addEventListener('click', () => {
    modal.classList.remove('active');
})
const tabs = document.querySelectorAll("[data-tab-target]");
const tabContents = document.querySelectorAll(".tab-content");

tabs.forEach((tab) => {
  tab.addEventListener("click", () => {
    tabs.forEach((tab) => {
      tab.classList.remove("active");
    });
    tab.classList.add("active");
    tabContents.forEach((tabContent) => {
      tabContent.classList.remove("active");
    });
    const target = document.querySelector(tab.dataset.tabTarget);
    target.classList.add("active");
  });
});
const form = document.querySelector('.payment__wrapper');
const number = document.querySelector('.number');
const name = document.querySelector('.name');
const date = document.querySelector('.date');
const cvv = document.querySelector('.cvv');

// Показать ошибку //

function showError(element, error) {
    if(error === true) {
        element.classList.add('error');
        element.classList.remove('success');
    } else {
        element.classList.remove('error');
        element.classList.add('success');
    }
};

// 1. Форматирование номера карты //

number.addEventListener('input', function(e) {
    this.value = numberAutoFormat();

    // показать ошибку, если отличается от 16 чисел и 3 пробелов //

    let error = this.value.length !== 19;
    let alert1 = document.querySelector('.alert1');
    showError(alert1, error)
})

function numberAutoFormat() {
    let valueNumber = number.value;
    // если пробелы меняются на ''. Если это не число от 0 до 9, измените его на ''
    let v = valueNumber.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
    // значение имеет минимум 4 цифры и максимум 16
    let matches = v.match(/\d{4,16}/g);
    let match = matches && matches[0] || '';
    let parts = [];

    for (i = 0; i < match.length; i += 4) {
        // после 4 цифр добавляем новый элемент в массив
        // e.g. "4510023" -> [4510, 023]
        parts.push(match.substring(i, i + 4));
    }

    if (parts.length) {
        // добавляем пробел после 4 цифр
        return parts.join(' ');
    } else {
        return valueNumber;
    }
};

// 2. Форматирование даты выпуска карты //

date.addEventListener('input', function(e) {
    this.value = dateAutoFormat();
    // показать ошибку, если дата недействительна
    let alert2 = document.querySelector('.alert2');
    showError(alert2, isNotDate(this));

    let dateNumber = date.value.match(/\d{2,4}/g);
})

function isNotDate(element) {
    let actualDate = new Date();
    let month = actualDate.getMonth() + 1; // начало января 0, нам нужно добавить + 1
    let year = Number(actualDate.getFullYear().toString().substr(-2)); // 2022 -> 22
    let dateNumber = element.value.match(/\d{2,4}/g);
    let monthNumber = Number(dateNumber);
    let yearNumber = Number(dateNumber);
    
    if(element.value === '' || monthNumber < 1 || monthNumber > 12 || yearNumber < year || (monthNumber <= month && yearNumber === year)) {
        return true;
    } else {
        return false;
    }
}

function dateAutoFormat() {
    let dateValue = date.value;
    // если пробел -> изменить на ''. Если это не число от 0 до 9 -> измените на ''
    let v = dateValue.replace(/\s+/g, '').replace(/[^0-9]/gi, '');

    // минимум 2 цифры и максимум 4
    let matches = v.match(/\d{2,4}/g);
    let match = matches && matches[0] || '';
    let parts = [];

    for (i = 0; i < match.length; i += 2) {
        // после 4 цифр добавляем новый элемент в массив
        // e.g. "4510023" -> [4510, 023]
        parts.push(match.substring(i, i + 2));
    }

    if (parts.length) {
        // добавляем пробел после 4 цифр
        return parts.join('/');
    } else {
        return dateValue;
    }
};

// 3. Форматирование владельца карты //

name.addEventListener('input', () => {
    let alert3 = document.querySelector('.alert3');
    let error = this.value === '';
    showError(alert3, error);
})

// 4. Форматирование CVV //

cvv.addEventListener('input', function(e) {
    let alert4 = document.querySelector('.alert4');
    let error = this.value.length < 3;
    showError(alert4, error)
});


// Проверка, является ли нажатая клавиша цифрой

function isNumeric(event) {
    if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode > 31)) {
        return false;
    }
};


// Валидация формы при клике //

form.addEventListener('submit', function(e) {
    // 1. если нет имени
    // 2. если длина числовой карты недопустима (16 цифр и 3 пробела)
    // 3. если дата недействительна (цифра 4 и "/" или дата недействительна)
    // 4. if не является допустимым cvv

    if(name.value === '' || number.value.length !== 19 || date.value.length !== 5 || isNotDate(date) === true || cvv.value.length < 3) {
        e.preventDefault();
    };

    // 5. если какой-либо ввод пуст, показать предупреждение об этом вводе

    let input = document.querySelectorAll('.card-input');
    for( i = 0; i < input.length; i++) {
        if(input[i].value === '') {
            input[i].classList.add('error');
        }
    }

    // 5. Активный чекбокс
    const checkbox = document.querySelector('.checkbox');
    const tearms = document.querySelector('.order__tearms');
    if (checkbox.checked) {
        tearms.classList.add('success');
        tearms.classList.remove('error');
    } else {
        tearms.classList.add('error');
        tearms.classList.remove('success');
    }
});